## The project implementation of a futures exchange(API app)

# Install

```
$ python3 -m pip install django
$ python3 -m pip install djangorestframework
$ python3 -m pip install drf-enum-field
$ python3 -m pip install djangorestframework-serializer-extensions

$ cd {root app}
$ python3 manager.py migrate
$ python3 manager.py runserver
```

# Enum

`Direction`

* BUY 
* SELL

- - -


# objects

`User`

```json
{
    id : 1 
    username : 'alex',
    email : 'super@gmail.com',
    password : '12345678' # only POST
}    

```

`Order`

```json
    {
        id : 1,
        lots : 10,
        price : 100.10
        direction : :direction
        asset : {},#expand
        user :  {} #expand
    }

```

`Asset`

```json
    {
        id : 1,
        name : 'Futures on the RTS index',
        margin : 100.0
    }

```

`OrderBook`
```json
{
    bids : [{price : 100.0, total_lots = 100 }....]
    asks : [.............
}
```

# methods

`GET` /api/users/:id

`responce` `User[]`

- - -

`GET` api/users/:id

`responce` : `User`

- - -

`GET /assets/:id/get-order-book`

`responce` :  `OrderBook`

- - -

`POST` api/orders/:id

`request` : `User`

`responce` :  `User`

- - -

`GET` /api/assets/:id

`responce` :  `Asset`

- - -

`GET` /api/assets/:id/get_order_book

`responce` :  `OrderBook`

