from . import views
from rest_framework import routers

router = routers.SimpleRouter(trailing_slash=False)

router.register(r'users', views.UserViewSet)
router.register(r'assets', views.AssetViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'deals', views.DealViewSet)

urlpatterns = router.urls
