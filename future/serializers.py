from rest_framework import serializers
from future.models import Asset, User, Order, Deal
from rest_framework.validators import UniqueValidator
from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework_serializer_extensions.serializers import SerializerExtensionsMixin, ExpandableFieldsMixin
from .services import ExecuteOrder


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'],
                                        validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')


class AssetSerializer(serializers.ModelSerializer):
    status = serializers.ChoiceField(choices=Asset.STATUS_CHOICES, read_only=True)

    class Meta:
        model = Asset
        fields = ('id', 'name', 'margin', 'date_execution', 'status')
        read_only_fields = ('ask', 'bid')


class OrderSerializer(ExpandableFieldsMixin, EnumFieldSerializerMixin, serializers.ModelSerializer):
    asset_id = serializers.PrimaryKeyRelatedField(source='asset', queryset=Asset.objects.all())
    user_id = serializers.PrimaryKeyRelatedField(source='user', queryset=User.objects.all())

    def create(self, validated_data):
        order = super(OrderSerializer, self).create(validated_data)
        ExecuteOrder(order.asset, order).execute()
        return order

    class Meta:
        model = Order
        fields = ('id', 'direction', 'price', 'lots', 'user_id', 'asset_id')

        expandable_fields = dict(
            asset=dict(
                serializer=AssetSerializer,
                read_only=True,
                id_source=False
            ),
            user=dict(
                serializer=UserSerializer,
                id_source=False
            ),
        )

class DealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deal
        read_only_fields = ('id', 'direction', 'price', 'lots')