from django.db import models
from django.db.models import DecimalField
from django.contrib.auth.models import AbstractUser, UserManager
from enumfields import EnumField
from enumfields import Enum


class User(AbstractUser):

    pass


class Asset(models.Model):
    STATUS_NEW = 0
    STATUS_Trading = 1
    STATUS_EXECUTED = 2
    STATUS_CHOICES = (
        (STATUS_NEW, 'New'),
        (STATUS_Trading, 'Trading'),
        (STATUS_EXECUTED, 'Executed'),
    )
    name = models.CharField(max_length=20)
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=STATUS_NEW)
    date_execution = models.DateField()
    bid = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    ask = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    margin = models.DecimalField(max_digits=8, decimal_places=2)


class Direction(Enum):
    BUY = 'b'
    SELL = 's'


class Order(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    asset = models.ForeignKey(Asset, models.CASCADE)
    direction = EnumField(Direction, max_length=1)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    lots = models.IntegerField()

    def is_buy(self): return Direction.BUY == self.direction

    def is_sell(self): return Direction.SELL == self.direction


class Deal(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    asset = models.ForeignKey(Asset, models.CASCADE)
    direction = EnumField(Direction, max_length=1)
    lots = models.IntegerField()
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)


class Position(models.Model):
    user = models.ForeignKey(User, models.CASCADE)
    asset = models.ForeignKey(Asset, models.CASCADE)
    lots = models.IntegerField(default=0)
