from .models import Order, User, Asset, Direction, Deal, Position
from django.db.models import Sum
from .queries import get_best_orders
from django.db.models import F


class DealCreator():
    def __init__(self, asset: Asset, buyer: User, seller: User, lots: int, price: float):
        self.buyer = buyer
        self.seller = seller
        self.asset = asset
        self.lots = lots
        self.price = price
        self.amount = price * lots

    def _create(self, user: User, direction: Direction):
        deal = Deal(
            asset=self.asset,
            user=user,
            direction=direction,
            lots=self.lots,
            amount=self.amount,
            price=self.price
        )
        deal.save()

    def _add_position(self, user: User, lots: int):
        position = user.position_set.filter(asset=self.asset).first()
        if (position == None):
            Position(asset=self.asset, user=user, lots=lots).save()
        else:
            position.lots = F('lots') + lots
            position.save()

    def create(self):
        self._create(self.buyer, Direction.BUY)
        self._create(self.seller, Direction.SELL)
        self._add_position(self.buyer, self.lots)
        self._add_position(self.seller, -self.lots)


class ExecuteOrder:
    def __init__(self, asset: Asset, order: Order):
        self.order = order
        self.asset = asset

    def execute(self):
        for best_order in get_best_orders(self.order):
            lots = min(self.order.lots, best_order.lots)
            if (lots > 0):
                self.make_deal(best_order, lots)
            else:
                break

    def _minus_lots(self, order: Order, lots):
        order.lots -= lots
        if (order.lots == 0):
            order.delete()

    def _get_seller_buyer(self, best_order: Order):
        if (best_order.is_buy()):
            return best_order.user, self.order.user
        else:
            return self.order.user, best_order.user

    def make_deal(self, best_order: Order, lots: int):
        self._minus_lots(self.order, lots)
        self._minus_lots(best_order, lots)
        buyer, seller = self._get_seller_buyer(best_order)
        DealCreator(self.asset, buyer, seller, lots, best_order.price).create()


class OrderBook():
    def __init__(self, asset: Asset):
        self.asset = asset

    def _prices(self):
        return Order.objects \
            .values('price') \
            .filter(asset=self.asset) \
            .annotate(total_lots=Sum('lots'))

    def asks(self):
        return self._prices().filter(direction=Direction.SELL).order_by('price')

    def bids(self):
        return self._prices().filter(direction=Direction.BUY).order_by('-price')
