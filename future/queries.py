from .models import Asset, Direction, Order

def get_best_buy_orders(asset: Asset):
    return Order.objects.filter(
        asset=asset,
        direction=Direction.BUY,
    ).order_by('price', 'id')

def get_best_sell_orders(asset: Asset):
    return Order.objects.filter(
        asset=asset,
        direction=Direction.SELL,
    ).order_by('-price', 'id')

def get_best_orders(order: Order):
    if (order.is_buy()):
        return get_best_sell_orders(order.asset) \
            .filter(price__lte=order.price)
    if (order.is_sell()):
        return get_best_buy_orders(order.asset) \
            .filter(price__gte=order.price)
