from .models import Asset, User, Order, Deal
from rest_framework_serializer_extensions.views import SerializerExtensionsAPIViewMixin
from future.serializers import AssetSerializer, UserSerializer, OrderSerializer, DealSerializer
from rest_framework.views import APIView
from rest_framework import generics, viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework.viewsets import ModelViewSet
from .services import OrderBook

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AssetViewSet(ModelViewSet):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer

    @detail_route(methods=['GET'])
    def get_order_book(self, request, pk=None):
        instance = self.get_object()
        orderbook = OrderBook(instance)
        return Response({
            'asks' : orderbook.asks(),
            'bids': orderbook.bids(),
        })

class OrderViewSet(SerializerExtensionsAPIViewMixin, ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class DealViewSet(ModelViewSet):
    queryset = Deal.objects.all()
    serializer_class = OrderSerializer